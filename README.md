# exceptional

![Hex.pm](https://img.shields.io/hexpm/dw/exceptional)
Error & exception handling helpers for Elixir [exceptional](https://hex.pm/packages/exceptional)

# Similar packages
* ![exceptional](https://img.shields.io/hexpm/dw/exceptional) exceptional
* ![ok](https://img.shields.io/hexpm/dw/ok) ok
* ![towel](https://img.shields.io/hexpm/dw/towel) towel
* ![witchcraft](https://img.shields.io/hexpm/dw/witchcraft) witchcraft
* ![monadex](https://img.shields.io/hexpm/dw/monadex) monadex
